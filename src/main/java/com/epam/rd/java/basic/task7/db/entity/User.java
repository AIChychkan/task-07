package com.epam.rd.java.basic.task7.db.entity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

public class User {

	private int id;
	private String login;

	public User(String login) {
		this.login = login;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db", "root", "root");
			Statement statement = connection.createStatement();

			String query = "INSERT INTO users "
					+ " (id, login)"
					+ " values(999, " + "'" + login + "'" + ")";//какая-то лажа с нулём
			statement.executeUpdate(query);
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}

		return new User(login);
	}

	@Override
	public String toString() {
		return login;
	}

	@Override
	public boolean equals(Object o) {
		User passedUser = (User)o;

		if (this.login.equals(passedUser.getLogin())) return true;
		if (o == null || getClass() != o.getClass()) return false; //ну хз

		return id == passedUser.id &&
				Objects.equals(login, passedUser.login);
	}
}
