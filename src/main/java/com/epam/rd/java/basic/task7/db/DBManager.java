package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {
/*
    private static final String URL = "jdbc:mysql://localhost:3306/test2db?autoReconnect=true&useSSL=false";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
*/

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            synchronized (DBManager.class) {
                if (instance == null) {
                    instance = new DBManager();
                }
            }
        }
        return instance;
    }

    private DBManager() {
        if (instance != null) {
            throw new RuntimeException("Already exists.");
        }
        System.out.println("Creating.");
    }

    public List<User> findAllUsers() throws DBException {
        String query = "SELECT login FROM users";
        List<User> loginsOfUsers = new ArrayList<>();

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db", "root", "root");
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String login = resultSet.getString("login");
//                assert false;
                loginsOfUsers.add(new User(login));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return loginsOfUsers;
    }

    public boolean insertUser(User user) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db", "root", "root");
            Statement statement = connection.createStatement();

            String query = "INSERT INTO users "
                    + " (id, login)"
                    + " values(default, " + "'" + user.getLogin() + "'" + ")";
            statement.executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        return false;
    }

    public User getUser(String login) {
        String query = "SELECT login FROM users WHERE login = '" + login + "'";

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db", "root", "root");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                String loginFromTableUsers = resultSet.getString("login");
                return new User(loginFromTableUsers);
            }
//            return new User(loginFromTableUsers);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    public Team getTeam(String name) {
        String query = "SELECT name FROM teams WHERE name = '" + name + "'";

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db", "root", "root");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                String nameFromTableTeams = resultSet.getString("name");
                return new Team(nameFromTableTeams);
            }
//            return new User(loginFromTableUsers);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    public List<Team> findAllTeams() {
        String query = "SELECT name FROM teams";
        List<Team> namesOfTeams = new ArrayList<>();

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db", "root", "root");
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
//                assert false;
                namesOfTeams.add(new Team(name));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return namesOfTeams;
    }

    public boolean insertTeam(Team team) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db", "root", "root");
            Statement statement = connection.createStatement();
            String query = "INSERT INTO teams "
                    + " (id, name)"
                    + " values(default, " + "'" + team.getName() + "'" + ")";

            statement.executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        return false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        return null;
    }

    public boolean deleteTeam(Team team) throws DBException {
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {
        return false;
    }

    private void readDatabase() throws IOException {
        String query = "SELECT login FROM users";
        List<String> loginsOfUsers = null;

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db", "root", "root");
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String login = resultSet.getString("login");
//                User newUser = new User(login);
                assert false;
                loginsOfUsers.add(login);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
