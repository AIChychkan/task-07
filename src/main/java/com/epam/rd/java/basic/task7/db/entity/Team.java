package com.epam.rd.java.basic.task7.db.entity;

import java.sql.*;
import java.util.Objects;

public class Team {
	private int id;
	private String name;

	public Team(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db", "root", "root");
			Statement statement = connection.createStatement();

			String query = "INSERT INTO teams "
					+ " (id, name)"
					+ " values(999, " + "'" + name;//какая-то лажа с нулём
			statement.executeUpdate(query);
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}

		return new Team(name);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		Team passedTeam = (Team)o;

		if (this.name.equals(passedTeam.getName())) return true;
		if (o == null || getClass() != o.getClass()) return false; //ну хз

		return id == passedTeam.id &&
				Objects.equals(name, passedTeam.name);
	}
}
